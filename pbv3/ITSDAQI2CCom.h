#ifndef ITSDAQI2CCOM_H
#define ITSDAQI2CCOM_H

#include <memory>

#include "I2CCom.h"
#include "ITSDAQCom.h"

#define ITSDAQ_I2C_CMD_START 0x0800
#define ITSDAQ_I2C_CMD_STOP 0x1000
#define ITSDAQ_I2C_CMD_SEND1 0x0400
#define ITSDAQ_I2C_CMD_READ1 0x0100
#define ITSDAQ_I2C_CMD_READ2 0x0200

class ITSDAQI2CCom : public I2CCom {
 public:
    ITSDAQI2CCom(uint8_t deviceAddr, std::shared_ptr<ITSDAQCom> com);
    virtual ~ITSDAQI2CCom();

    //
    // Write commands
    virtual void write_reg32(uint32_t address, uint32_t data);
    virtual void write_reg24(uint32_t address, uint32_t data);
    virtual void write_reg16(uint32_t address, uint16_t data);
    virtual void write_reg8(uint32_t address, uint8_t data);

    virtual void write_reg32(uint32_t data);
    virtual void write_reg24(uint32_t data);
    virtual void write_reg16(uint16_t data);
    virtual void write_reg8(uint8_t data);

    virtual void write_block(uint32_t address,
                             const std::vector<uint8_t> &data);
    virtual void write_block(const std::vector<uint8_t> &data);

    //
    // Read commands
    virtual uint32_t read_reg32(uint32_t address);
    virtual uint32_t read_reg24(uint32_t address);
    virtual uint16_t read_reg16(uint32_t address);
    virtual uint8_t read_reg8(uint32_t address);

    virtual uint32_t read_reg32();
    virtual uint32_t read_reg24();
    virtual uint16_t read_reg16();
    virtual uint8_t read_reg8();

    virtual void read_block(uint32_t address, std::vector<uint8_t> &data);
    virtual void read_block(std::vector<uint8_t> &data);

    //
    // Exclusive access control

    //! TO BE IMPLEMENTED, DUMMY FOR NOW
    virtual void lock();

    //! TO BE IMPLEMENTED, DUMMY FOR NOW
    virtual void unlock();

 private:
    std::shared_ptr<ITSDAQCom> m_com;
};

#endif  // ITSDAQI2CCOM_H
