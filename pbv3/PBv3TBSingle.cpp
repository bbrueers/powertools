#include "PBv3TBSingle.h"

#include <NotSupportedException.h>
#include <OutOfRangeException.h>

#include "EndeavourRawFTDI.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

// Register test bench
#include "PBv3TBRegistry.h"
REGISTER_PBV3TB(PBv3TBSingle)

void PBv3TBSingle::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "bkdev") {
            m_bkdev = kv.value();
        }
        if (kv.key() == "ftdi_serial") {
            m_ftdi_serial = kv.value();
        }
        if (kv.key() == "ftdi_description") {
            m_ftdi_description = kv.value();
        }
    }

    PBv3TB::setConfiguration(config);
}

void PBv3TBSingle::init() {
    if (!m_bkdev.empty()) initLoad();
    initPB();
}

void PBv3TBSingle::initLoad() {
    //
    // Configure variable load
    logger(logINFO) << "Init BK DCDC Load";
    m_load = std::make_shared<Bk85xx>(m_bkdev);

    m_load->setRemote();
    m_load->setRemoteSense(false);
    m_load->setModeCC();
    m_load->setCurrent(0);
}

void PBv3TBSingle::initPB() {
    //
    // Configure the AMAC
#ifdef FTDI
    static const uint16_t amacid = 0x0;

    m_pb = std::make_shared<AMACv2>(
        amacid, std::unique_ptr<EndeavourRaw>(
                    new EndeavourRawFTDI(m_ftdi_description, m_ftdi_serial)));

    setCalDAC(dynamic_cast<EndeavourRawFTDI *>(m_pb->raw().get())->getDAC());
#else
    throw NotSupportedException("PB communication not supported without FTDI.");
#endif
}

std::shared_ptr<AMACv2> PBv3TBSingle::getPB(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);
    return m_pb;
}

void PBv3TBSingle::setOFin(uint8_t pbNum, bool value) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);
#ifdef FTDI
    dynamic_cast<EndeavourRawFTDI *>(m_pb->raw().get())->setOF(value);
#else
    throw NotSupportedException("PB communication not supported without FTDI.");
#endif
}

double PBv3TBSingle::getVin() { return getLVPS()->measureVoltage(); }

double PBv3TBSingle::getNTC(uint8_t ntc) {
    logger(logWARNING) << "getNTC is not implemented on single yet.";
    return 0.;
}
double PBv3TBSingle::getActiveTemp() {
    logger(logWARNING) << "ActiveTemp is not implemented on single yet.";
    return 0.;
}

double PBv3TBSingle::getActiveHum() {
    logger(logWARNING) << "ActiveHum is not implemented on single yet.";
    return 0.;
}

double PBv3TBSingle::getActiveDew() {
    logger(logWARNING) << "ActiveDew is not implemented on single yet.";
    return 0.;
}

double PBv3TBSingle::getIload(uint8_t pbNum) { return m_load->getValues().cur; }

std::shared_ptr<Bk85xx> PBv3TBSingle::getLoadPtr() { return m_load; }

void PBv3TBSingle::loadOn(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    m_load->turnOn();
}

void PBv3TBSingle::loadOff(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    m_load->turnOff();
}

double PBv3TBSingle::setLoad(uint8_t pbNum, double load) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    return 0.;  // TODO implement
}

double PBv3TBSingle::getLoad(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    return m_load->getValues().cur * 1e3;
}

double PBv3TBSingle::getVout(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    return m_load->getValues().vol * 1e3;
}

double PBv3TBSingle::getHVoutCurrent(uint8_t pbNum) {
    if (pbNum != 0) throw OutOfRangeException(pbNum, 0, 0);

    return 0.;  // TODO implement
}

double PBv3TBSingle::readCarrierOutput(uint32_t pbNum,
                                       PBv3TB::CARRIER_OUTPUT value) {
    logger(logWARNING)
        << "Powerboard output reading not setup for single testbench.";

    return 0.;
}
