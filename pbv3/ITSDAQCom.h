#ifndef ITSDAQCOM_H_
#define ITSDAQCOM_H_

#include <netinet/in.h>

#include <cstdint>
#include <string>

#include "ITSDAQPacket.h"

class ITSDAQCom {
 public:
    ITSDAQCom(const std::string &addr, uint32_t port);
    ~ITSDAQCom();

    void send(const ITSDAQPacket &packet);
    ITSDAQPacket receive();

 private:
    int32_t m_socket = 0;
    struct sockaddr_in m_fpgaaddr;
};

#endif  // ITSDAQCom
