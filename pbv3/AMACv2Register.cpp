#include "AMACv2Register.h"

AMACv2Register::AMACv2Register(const std::string &name, uint32_t address,
                               rw_t rw)
    : m_name(name), m_address(address), m_rw(rw) {}

bool AMACv2Register::isValid() const { return !m_name.empty(); }

std::string AMACv2Register::getName() const { return m_name; }

uint8_t AMACv2Register::getAddress() const { return m_address; }

void AMACv2Register::setValue(uint32_t value) { m_value = value; }

uint32_t AMACv2Register::getValue() const { return m_value; }

rw_t AMACv2Register::isRW() const { return m_rw; }
