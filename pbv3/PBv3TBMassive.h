#ifndef PBV3TBMASSIVE_H
#define PBV3TBMASSIVE_H

#include <memory>

#include "ADCDevice.h"
#include "ClimateSensor.h"
#include "DACDevice.h"
#include "I2CCom.h"
#include "IOExpander.h"
#include "MuxDevice.h"
#include "PBv3TB.h"
#include "UIOCom.h"

#define PBV3_ADC_CH_VIN 3
#define PBV3_ADC_CH_VIN_CURR 0
#define PBV3_ADC_CH_P5V_CURR 1
#define PBV3_ADC_CH_M5V_CURR 2

#define DAC_CH_LOAD_PB0 1
#define DAC_CH_LOAD_PB1 2
#define DAC_CH_LOAD_PB2 0
#define DAC_CH_LOAD_PB3 3
#define DAC_CH_LOAD_PB4 1
#define DAC_CH_LOAD_PB5 2
#define DAC_CH_LOAD_PB6 0
#define DAC_CH_LOAD_PB7 3
#define DAC_CH_LOAD_PB8 1
#define DAC_CH_LOAD_PB9 2

#define ADC_VOUT_PB0 7
#define ADC_VOUT_PB1 5
#define ADC_VOUT_PB2 6
#define ADC_VOUT_PB3 4
#define ADC_VOUT_PB4 2
#define ADC_VOUT_PB5 0
#define ADC_VOUT_PB6 7
#define ADC_VOUT_PB7 5
#define ADC_VOUT_PB8 6
#define ADC_VOUT_PB9 4

#define ADC_HVOUT_PB1 2
#define ADC_HVOUT_PB2 3
#define ADC_HVOUT_PB3 0
#define ADC_HVOUT_PB4 1
#define ADC_HVOUT_PB5 2
#define ADC_HVOUT_PB6 3
#define ADC_HVOUT_PB7 0
#define ADC_HVOUT_PB8 1
#define ADC_HVOUT_PB9 2

#define ADC_ILOAD_PB0 1
#define ADC_ILOAD_PB1 2
#define ADC_ILOAD_PB2 0
#define ADC_ILOAD_PB3 3
#define ADC_ILOAD_PB4 1
#define ADC_ILOAD_PB5 2
#define ADC_ILOAD_PB6 0
#define ADC_ILOAD_PB7 3
#define ADC_ILOAD_PB8 1
#define ADC_ILOAD_PB9 2

#define ADC_NTC_CH0 3
#define ADC_NTC_CH1 1
#define ADC_NTC_CH2 3
#define ADC_NTC_CH3 1

#define AMP_HV_PB1 0
#define AMP_HV_PB2 1
#define AMP_HV_PB3 2
#define AMP_HV_PB4 3
#define AMP_HV_PB5 4
#define AMP_HV_PB6 5
#define AMP_HV_PB7 6
#define AMP_HV_PB8 7
#define AMP_HV_PB9 8

//! Testbench used for mass testing of Powerboards
/**
 * Meant to be used with the Active Board used for
 * mass testing of Powerboards during electrical QC.
 *
 * This class handles the common functionality of the
 * multiple revisions of the active boards. The actual
 * active boards are implemented in classes called
 * `PBv3TBMassiveDATE`, where `DATE` is the version
 * written on the board silkscreen.
 *
 * The main difference between Active Board revisions
 * are the chip part numbers used for specific measurements.
 * The subclasses should handle this by creating the right
 * chip class in `initDevices` and assigning it to the right
 * member.
 *
 * The chip parts that are known to be the same between
 * Active Board revisions are initialized inside this class.
 */
class PBv3TBMassive : public PBv3TB {
 public:
    PBv3TBMassive(std::shared_ptr<PowerSupplyChannel> lv,
                  std::shared_ptr<PowerSupplyChannel> hv);
    PBv3TBMassive(std::shared_ptr<EquipConf> hw);
    PBv3TBMassive() = default;
    ~PBv3TBMassive() = default;

    //! Configure on JSON object
    /**
     * Valid keys:
     *  - `i2cdev`: Path to I2C device
     *  - `hvcurrmuxdev`: Path to SPI device for the HV current multiplexer
     *  - `pbdev`: UIO device with endeavour firmware when running in daisy
     * chain mode, empty for direct connection
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json &config);

    //! Initialize testbench
    /**
     * Calls `initDevices` and `loadCalibrations`.
     */
    virtual void init();

    /**
     * Disable carrier card multiplexers if a carrier card is
     * plugged in. If not, then print a warning.
     */
    virtual void powerTBOn();

    std::shared_ptr<AMACv2> getPB(uint8_t pb);

    void setOFin(uint8_t pbNum, bool value);

    double getVin();
    double getP5VCurrent();
    double getM5VCurrent();

    double getNTC(uint8_t ntc);
    double getActiveTemp();
    double getActiveHum();
    double getActiveDew();

    void loadOn(uint8_t pbNum);
    void loadOff(uint8_t pbNum);
    double setLoad(uint8_t pbNum, double load);
    void setLoadCounts(uint8_t pbNum, uint32_t counts);
    double getLoad(uint8_t pbNum);

    double getVout(uint8_t pbNum);
    double getIload(uint8_t pbNum);
    uint32_t getVoutCount(uint8_t pbNum);

    double getHVoutCurrent(uint8_t pbNum);

    void setCarrierOutput(PBv3TB::CARRIER_OUTPUT value);
    void setCarrierOutputEnable(uint32_t pbNum, bool value);

    /*
     * Reads the value of the multiplexed carrier output.
     * The multiplexer is enable and then disabled after
     * the value is read.
     *
     * \param value The channel to read
     *
     * \return Measured value in volts
     */
    double readCarrierOutput(uint32_t pbNum, PBv3TB::CARRIER_OUTPUT value);

    //! Determine if a carrier card is plugged in
    /**
     * Presence of a carrier card is determied by reading
     * the contents of a I2C device (mux ch2, address 0x20)
     * on the card. A failed I2C communication defines a
     * missing carrier card.
     */
    virtual bool checkCarrierCard();

    //! \brief Get I2C multiplexer serving as root for all I2C devices
    std::shared_ptr<I2CCom> getI2CRoot();

 protected:
    //! i2cdev Linux device to serve as I2C root
    std::string m_i2cdev = "/dev/i2c-0";

    //! hvcurrmuxdev Linux device for the HV currense sense mux
    std::string m_hvcurrmuxdev = "/dev/spidev1.0";

    //! pbdev Linux device corresponding to the endeavour block
    std::string m_pbdev;

 protected:
    //! Initialize chips on the active board
    /**
     * Subclasses should override this function to
     * initialize any revision-specific chip models
     * and then call this implenentation to handle
     * the common chips.
     */
    virtual void initDevices();

    //! Applies calibrations to devices
    virtual void loadCalibrations();

    //
    // I2C multiplexers
    std::shared_ptr<I2CCom> m_i2c_root;

    std::shared_ptr<I2CCom> m_i2c_analogue;

    // SPI device
    std::shared_ptr<MuxDevice> m_mux_hv;

    // I2C devices
    std::shared_ptr<ADCDevice> m_adc_pwr;
    std::shared_ptr<ADCDevice> m_adc_lv0;
    std::shared_ptr<ADCDevice> m_adc_lv1;
    std::shared_ptr<ADCDevice> m_adc_lvset0;
    std::shared_ptr<ADCDevice> m_adc_lvset1;
    std::shared_ptr<ADCDevice> m_adc_lvset2;
    std::shared_ptr<ADCDevice> m_adc_hv;
    std::shared_ptr<ADCDevice> m_adc_common;

    std::shared_ptr<DACDevice> m_dac_0;
    std::shared_ptr<DACDevice> m_dac_1;
    std::shared_ptr<DACDevice> m_dac_2;

    std::shared_ptr<IOExpander> m_sel_out;
    std::shared_ptr<IOExpander> m_sel_of;

    std::shared_ptr<IOExpander> m_pbsel;

    /// Powerboards
    std::vector<std::shared_ptr<AMACv2>> m_pbs{10};

    // Climate sensors
    std::shared_ptr<ClimateSensor> m_ntc0;
    std::shared_ptr<ClimateSensor> m_ntc1;
    std::shared_ptr<ClimateSensor> m_ntc2;
    std::shared_ptr<ClimateSensor> m_ntc3;

    std::shared_ptr<ClimateSensor> m_sht;
};

#endif  // PBV3TBMASSIVE_H
