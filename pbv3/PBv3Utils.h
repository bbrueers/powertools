#ifndef PBV3UTILS_H
#define PBV3UTILS_H

#include <chrono>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>

namespace PBv3Utils {

//! \brief Formats the time in a human-readable format.
/**
 * The time is formatted as follows: YEAR_MONTH_DAY-HOUR:MINUTE:SECOND
 *
 * \param t time point
 *
 * \return string containing date/time
 */
std::string getTimeAsString(std::chrono::system_clock::time_point t);

//! \brief Pretty print results stored in a JSON file
/**
 * Prints the different columns in the results structure in
 * a table. The text is formatted such that all columns
 * align nicely.
 *
 * If an field is an array, then multiple rows are  printed.
 * In this case, scalar fields are repeated. There is an
 * assumption that all arrays are of the same length.
 *
 * The following columns (keys of `results`) are ignored:
 *  - TIMESTART
 *  - TIMEEND
 *
 * \param results JSON dictionary containing results
 */
void printResults(const nlohmann::json &results);

//! \brief Create directory at path, if it does not already exist
/**
 * All parent directories must already exist.
 *
 * \param path Path for directory.
 */
void createDirectory(const std::string &path);

//! Merge test results
/**
 * Logic to merge is as follows:
 * - `/passed` is the AND of the two inputs. This field must already be present
 * in both.
 * - Contents of base is updated using `merge_patch`.
 *
 * \param baseResult Base result that will be updated in-place
 * \param addResult New data that will be merged into `baseResult`
 */
void mergeResult(nlohmann::json &baseResult, const nlohmann::json &addResult);
}  // namespace PBv3Utils

#endif  // PBV3UTILS_H
