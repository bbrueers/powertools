#include <Logger.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

//------ Channel Map
std::map<std::string, AMACv2::AM> channelmap = {
    {"VDCDC", AMACv2::AM::VDCDC},         {"VDDLR", AMACv2::AM::VDDLR},
    {"VDDLRLo", AMACv2::AM::VDDLRLo},     {"VDDLRHi", AMACv2::AM::VDDLRHi},
    {"DCDCIN", AMACv2::AM::DCDCIN},       {"VDDREG", AMACv2::AM::VDDREG},
    {"VDDBG", AMACv2::AM::VDDBG},         {"AM900BG", AMACv2::AM::AM900BG},
    {"AM600BG", AMACv2::AM::AM600BG},     {"CAL", AMACv2::AM::CAL},
    {"AMREF", AMACv2::AM::AMREF},         {"CALX", AMACv2::AM::CALX},
    {"CALY", AMACv2::AM::CALY},           {"SHUNTX", AMACv2::AM::SHUNTX},
    {"SHUNTY", AMACv2::AM::SHUNTY},       {"DCDCADJ", AMACv2::AM::DCDCADJ},
    {"CTAT", AMACv2::AM::CTAT},           {"NTCX", AMACv2::AM::NTCX},
    {"NTCY", AMACv2::AM::NTCY},           {"NTCPB", AMACv2::AM::NTCPB},
    {"HREFX", AMACv2::AM::HREFX},         {"HREFY", AMACv2::AM::HREFY},
    {"CUR10V", AMACv2::AM::CUR10V},       {"CUR10VTPL", AMACv2::AM::CUR10VTPL},
    {"CUR10VTPH", AMACv2::AM::CUR10VTPH}, {"CUR1V", AMACv2::AM::CUR1V},
    {"CUR1VTPL", AMACv2::AM::CUR1VTPL},   {"CUR1VTPH", AMACv2::AM::CUR1VTPH},
    {"HVRET", AMACv2::AM::HVRET},         {"PTAT", AMACv2::AM::PTAT},
};

//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] channel" << std::endl;
    std::cerr
        << "Channel can be either number or one of the following. a name.:"
        << std::endl;
    for (const std::pair<std::string, AMACv2::AM> &kv : channelmap)
        std::cerr << "  " << kv.first << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    std::string chStr;
    if (optind == argc - 1) {
        chStr = argv[optind];
    } else {
        std::cerr << "Required channel argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    tb->getPB(pbNum)->init();

    //
    // Perform the read of the ADC

    // try channel number
    try {
        int32_t chNum = std::stoi(chStr);

        AMACv2Field AMACv2RegMap::*ch;
        switch (chNum) {
            case 0:
                ch = &AMACv2::Ch0Value;
                break;
            case 1:
                ch = &AMACv2::Ch1Value;
                break;
            case 2:
                ch = &AMACv2::Ch2Value;
                break;
            case 3:
                ch = &AMACv2::Ch3Value;
                break;
            case 4:
                ch = &AMACv2::Ch4Value;
                break;
            case 5:
                ch = &AMACv2::Ch5Value;
                break;
            case 6:
                ch = &AMACv2::Ch6Value;
                break;
            case 7:
                ch = &AMACv2::Ch7Value;
                break;
            case 8:
                ch = &AMACv2::Ch8Value;
                break;
            case 9:
                ch = &AMACv2::Ch9Value;
                break;
            case 10:
                ch = &AMACv2::Ch10Value;
                break;
            case 11:
                ch = &AMACv2::Ch11Value;
                break;
            case 12:
                ch = &AMACv2::Ch12Value;
                break;
            case 13:
                ch = &AMACv2::Ch13Value;
                break;
            case 14:
                ch = &AMACv2::Ch14Value;
                break;
            case 15:
                ch = &AMACv2::Ch15Value;
                break;
            default:
                std::cerr << "Channel must be in range 0-15." << std::endl;
                usage(argv);
                return 1;
        }

        std::cout << tb->getPB(pbNum)->rdField(ch) << std::endl;
    } catch (const std::invalid_argument &e) {  // Could be a channel name
        if (channelmap.find(chStr) == channelmap.end()) {
            std::cerr << "Invalid channel name." << std::endl;
            usage(argv);
            return 1;
        }

        std::cout << tb->getPB(pbNum)->readAM(channelmap[chStr], 1000)
                  << std::endl;
    }

    return 0;
}
