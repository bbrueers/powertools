#include <Logger.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>

#include "EndeavourComException.h"
#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0]
              << " [options] command [command parameters]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "commands" << std::endl;
    std::cerr << " reset: reset the Endeavour master firmware" << std::endl;
    std::cerr << " setid idpads/efuse internalid: set amacid for internalid "
                 "efuse or idpads"
              << std::endl;
    std::cerr << " write address data: write data to address " << std::endl;
    std::cerr << " read address: read address " << std::endl;
    std::cerr << " readnext: read next register " << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    // Determine the command
    if (argc <= optind) {
        usage(argv);
        return 1;
    }

    std::string command = argv[optind++];
    std::vector<std::string> params;
    while (optind < argc) {
        std::string p(argv[optind++]);
        params.push_back(p);
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    // Run the commands
    try {
        if (command == "reset") {
            amac->reset();
        } else if (command == "setid") {
            // Need two more
            if (params.size() < 2) {
                usage(argv);
                return 1;
            }

            EndeavourCom::REFMODE refmode =
                (params[0] == "idpads") ? EndeavourCom::REFMODE::IDPads
                                        : EndeavourCom::REFMODE::EfuseId;
            unsigned int refid = std::stoul(params[1], nullptr, 0);

            amac->setid(refmode, refid);
        } else if (command == "write") {
            // Need two more
            if (params.size() < 2) {
                usage(argv);
                return 1;
            }

            unsigned int address = std::stoul(params[0], nullptr, 0);
            unsigned int data = std::stoul(params[1], nullptr, 0);

            amac->write_reg(address, data);
        } else if (command == "read") {
            // Need one more
            if (params.size() < 1) {
                usage(argv);
                return 1;
            }

            unsigned int address = std::stoul(params[0], nullptr, 0);

            unsigned int read = amac->read_reg(address);
            std::cout << "0x" << std::hex << std::setw(8) << std::setfill('0')
                      << read << std::endl;
        } else if (command == "readnext") {
            // Need one more
            if (params.size() > 0) {
                usage(argv);
                return 1;
            }

            unsigned int read = amac->readnext_reg();
            std::cout << "0x" << std::hex << std::setw(8) << std::setfill('0')
                      << read << std::endl;
        }
    } catch (EndeavourComException e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
