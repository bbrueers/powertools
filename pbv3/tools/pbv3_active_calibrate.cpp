#include <getopt.h>

#include <chrono>
#include <fstream>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <thread>

#include "DAC5574.h"
#include "EquipConf.h"
#include "I2CDevCom.h"
#include "IPowerSupply.h"
#include "Logger.h"
#include "MCP3428.h"
#include "PBv3TBMassive.h"
#include "PCA9548ACom.h"
#include "PowerSupplyChannel.h"
using nlohmann::json;

//------ SETTINGS
loglevel_e loglevel = logINFO;
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_calib.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] filename" << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number to test (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                loglevel = logDEBUG;
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Not enough parameters!" << std::endl;
        usage(argv);
        return 1;
    }

    std::string fileName = argv[optind++];

    //
    // Initialize the var load
    std::shared_ptr<I2CCom> i2c_root =
        std::make_shared<I2CDevCom>(0x70, "/dev/i2c-0");

    // Load measure
    std::shared_ptr<MCP3428> adc_lvset0 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x68, 5, i2c_root));
    std::shared_ptr<MCP3428> adc_lvset1 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x6C, 5, i2c_root));
    std::shared_ptr<MCP3428> adc_lvset2 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x6A, 5, i2c_root));

    // Load set
    std::shared_ptr<DAC5574> dac_0 = std::make_shared<DAC5574>(
        3.25, std::make_shared<PCA9548ACom>(0x4C, 5, i2c_root));
    std::shared_ptr<DAC5574> dac_1 = std::make_shared<DAC5574>(
        3.25, std::make_shared<PCA9548ACom>(0x4D, 5, i2c_root));
    std::shared_ptr<DAC5574> dac_2 = std::make_shared<DAC5574>(
        3.25, std::make_shared<PCA9548ACom>(0x4E, 5, i2c_root));

    //
    // Initialize power supplies and other equipment
    logger(logINFO) << "Init PS";
    EquipConf hw;
    hw.setHardwareConfig(equipConfigFile);

    std::shared_ptr<PowerSupplyChannel> ps = hw.getPowerSupplyChannel("FakePB");
    if (ps == nullptr) {
        logger(logERROR) << "Unable to fetch FakePB power supply!";
        return 2;
    }

    // Set inital values
    ps->setVoltageLevel(1.5);
    ps->setCurrentProtect(5);

    //
    // Initialize the output
    logger(logINFO) << "Results stored in " << fileName;
    std::fstream outfile(fileName, std::ios::out);
    if (!outfile.is_open()) {
        logger(logERROR) << "Unable to create results file " << fileName;
        return 2;
    }

    // measurement
    std::cout << "idx"
              << "\t"
              << "ISETcounts"
              << "\t"
              << "IPS"
              << "\t"
              << "ISENSE1"
              << "\t"
              << "ISENSE2"
              << "\t"
              << "ISENSE4"
              << "\t"
              << "ISENSE8"
              << "\t"
              << "ISET" << std::endl;
    outfile << "idx"
            << "\t"
            << "ISETcounts"
            << "\t"
            << "IPS"
            << "\t"
            << "ISENSE1"
            << "\t"
            << "ISENSE2"
            << "\t"
            << "ISENSE4"
            << "\t"
            << "ISENSE8"
            << "\t"
            << "ISET" << std::endl;

    //
    // Start testing
    ps->turnOn();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    logger(logINFO) << "Power supply turned on, voltage set to "
                    << ps->measureVoltage() << "V"
                    << ", current set to " << ps->measureCurrent() << "A";

    double RLoad = 25.0e-3;
    double R1 = 100e3;
    double R2 = 5e3;
    for (uint32_t DACcounts = 0; DACcounts < 0xAC; DACcounts += 2) {
        // set load counts
        switch (pbNum) {
            case 0:
                dac_0->setCount(DAC_CH_LOAD_PB0, DACcounts);
                break;

            case 1:
                dac_0->setCount(DAC_CH_LOAD_PB1, DACcounts);
                break;

            case 2:
                dac_0->setCount(DAC_CH_LOAD_PB2, DACcounts);
                break;

            case 3:
                dac_0->setCount(DAC_CH_LOAD_PB3, DACcounts);
                break;

            case 4:
                dac_1->setCount(DAC_CH_LOAD_PB4, DACcounts);
                break;

            case 5:
                dac_1->setCount(DAC_CH_LOAD_PB5, DACcounts);
                break;

            case 6:
                dac_1->setCount(DAC_CH_LOAD_PB6, DACcounts);
                break;

            case 7:
                dac_1->setCount(DAC_CH_LOAD_PB7, DACcounts);
                break;

            case 8:
                dac_2->setCount(DAC_CH_LOAD_PB8, DACcounts);
                break;

            case 9:
                dac_2->setCount(DAC_CH_LOAD_PB9, DACcounts);
                break;

            default:
                // TODO throw exception
                break;
        }

        for (uint32_t j = 0; j < 100; j++) {
            std::this_thread::sleep_for(std::chrono::seconds(1));

            double cur = ps->measureCurrent();

            std::cout << j << "\t" << DACcounts << "\t" << cur << "\t";
            outfile << j << "\t" << DACcounts << "\t" << cur << "\t";

            for (const MCP3428::Gain &gain :
                 {MCP3428::Gain::x1, MCP3428::Gain::x2, MCP3428::Gain::x4,
                  MCP3428::Gain::x8}) {
                adc_lvset0->setGain(gain);
                adc_lvset1->setGain(gain);
                adc_lvset2->setGain(gain);
                int32_t Iload = 0;
                switch (pbNum) {
                    case 0:
                        Iload = adc_lvset0->readCount(ADC_ILOAD_PB0);
                        break;

                    case 1:
                        Iload = adc_lvset0->readCount(ADC_ILOAD_PB1);
                        break;

                    case 2:
                        Iload = adc_lvset0->readCount(ADC_ILOAD_PB2);
                        break;

                    case 3:
                        Iload = adc_lvset0->readCount(ADC_ILOAD_PB3);
                        break;

                    case 4:
                        Iload = adc_lvset1->readCount(ADC_ILOAD_PB4);
                        break;

                    case 5:
                        Iload = adc_lvset1->readCount(ADC_ILOAD_PB5);
                        break;

                    case 6:
                        Iload = adc_lvset1->readCount(ADC_ILOAD_PB6);
                        break;

                    case 7:
                        Iload = adc_lvset1->readCount(ADC_ILOAD_PB7);
                        break;

                    case 8:
                        Iload = adc_lvset2->readCount(ADC_ILOAD_PB8);
                        break;

                    case 9:
                        Iload = adc_lvset2->readCount(ADC_ILOAD_PB9);
                        break;

                    default:
                        // TODO throw exception
                        Iload = 0.;
                        break;
                }

                std::cout << Iload << "\t";
                outfile << Iload << "\t";
            }

            double DACV = DACcounts * 3.25 / 255.0;

            std::cout << DACV * R2 / (R1 + R2) / RLoad << std::endl;
            outfile << DACV * R2 / (R1 + R2) / RLoad << std::endl;
        }
    }

    ps->turnOff();

    outfile.close();

    return 0;
}
