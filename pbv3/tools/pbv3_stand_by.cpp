#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "Logger.h"
#include "PBv3ActiveTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBMassive.h"
#include "PBv3Utils.h"
#include "PowerSupplyChannel.h"

//------ SETTINGS
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

bool skiphv = false;

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << "     --skiphv            Skip running HV tests" << std::endl;
    std::cerr
        << "     --hbipc             Specialized hybrid burn-in powerboard "
           "carrierboard tests"
        << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"skiphv  ", no_argument, 0, 1},
            {"hbipc", no_argument, 0, 2},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "e:h:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'e':
                equipConfigFile = optarg;
                break;
            case 1:
                skiphv = true;
                break;
            case 2:
                skiphv = true;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    logger(logINFO) << " Performing PS and active board checks";
    //
    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    // std::shared_ptr<PBv3TB> tb=factory_pbv3tb.getPBv3TB("default");
    std::shared_ptr<PBv3TBMassive> tb =
        std::dynamic_pointer_cast<PBv3TBMassive>(
            factory_pbv3tb.getPBv3TB("default"));
    if (tb == nullptr) {
        logger(logERROR) << "Error obtaining testbench object!";
        return 1;
    }

    if (tb->checkCarrierCard())
        logger(logERROR)
            << "Please unplug carrier card before PS initilization!!";
    else
        PBv3ActiveTools::testPSINIT(tb, skiphv);
    PBv3ActiveTools::testI2C(tb);
    logger(logINFO) << " Power everything off";
    if (!skiphv) tb->powerHVOff();
    tb->powerLVOff();
    tb->powerTBOff();

    return 0;
}
