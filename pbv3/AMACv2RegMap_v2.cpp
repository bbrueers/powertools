#include "AMACv2RegMap.h"

void AMACv2RegMap::init_v2() {
    /** \name Registers
     * @{ */
    // RegisterStatus = {"Status", 0, RO};
    RegisterHxFlags = {"HxFlags", 1, RO};
    RegisterHyFlags = {"HyFlags", 2, RO};
    RegisterHV0Flags = {"HV0Flags", 3, RO};
    RegisterHV2Flags = {"HV2Flags", 4, RO};
    RegisterDCDCFlags = {"DCDCFlags", 5, RO};
    RegisterWRNHxFlags = {"WRNHxFlags", 6, RO};
    RegisterSynFlags = {"SynFlags", 7, RO};

    RegisterValue0 = {"Value0", 10, RO};
    RegisterValue1 = {"Value1", 11, RO};
    RegisterValue2 = {"Value2", 12, RO};
    RegisterValue3 = {"Value3", 13, RO};
    RegisterValue4 = {"Value4", 14, RO};
    RegisterValue5 = {"Value5", 15, RO};

    RegisterLogicReset = {"LogicReset", 33, WO};
    RegisterHardReset = {"HardReset", 34, WO};

    RegisterCntSet = {"CntSet", 40, RW};
    RegisterCntSetC = {"CntSetC", 41, RW};
    RegisterDCDCen = {"DCDCen", 42, RW};
    RegisterDCDCenC = {"DCDCenC", 43, RW};
    RegisterIlock = {"Ilock", 44, RW};
    RegisterIlockC = {"IlockC", 45, RW};
    RegisterRstCnt = {"RstCnt", 46, RW};
    RegisterRstCntC = {"RstCntC", 47, RW};
    RegisterAMen = {"AMen", 48, RW};
    RegisterAMenC = {"AMenC", 49, RW};
    RegisterAMpwr = {"AMpwr", 50, RW};
    RegisterAMpwrC = {"AMpwrC", 51, RW};
    RegisterBgCnt = {"BgCnt", 52, RW};
    RegisterAMCnt = {"AMCnt", 53, RW};
    RegisterDACs0 = {"DACs0", 54, RW};
    RegisterDACbias = {"DACbias", 55, RW};
    RegisterAMACCnt = {"AMACCnt", 56, RW};
    RegisterNTCRange = {"NTCRange", 57, RW};
    RegisterLVCurCal = {"LVCurCal", 58, RW};

    RegisterHxICfg = {"HxICfg", 60, RW};
    RegisterHyICfg = {"HyICfg", 61, RW};
    RegisterHV0ICfg = {"HV0ICfg", 62, RW};
    RegisterHV2ICfg = {"HV2ICfg", 63, RW};
    RegisterDCDCICfg = {"DCDCICfg", 64, RW};
    RegisterWRNICfg = {"WRNICfg", 65, RW};

    RegisterHxTLUT = {"HxTLUT", 70, RW};
    RegisterHxModLUT1 = {"HxModLUT1", 71, RW};
    RegisterHxModLUT2 = {"HxModLUT2", 72, RW};
    RegisterHyTLUT = {"HyTLUT", 73, RW};
    RegisterHyModLUT1 = {"HyModLUT1", 74, RW};
    RegisterHyModLUT2 = {"HyModLUT2", 75, RW};
    RegisterHV0TLUT = {"HV0TLUT", 76, RW};
    RegisterHV0ModLUT1 = {"HV0ModLUT1", 77, RW};
    RegisterHV0ModLUT2 = {"HV0ModLUT2", 78, RW};
    RegisterHV2TLUT = {"HV2TLUT", 79, RW};
    RegisterHV2ModLUT1 = {"HV2ModLUT1", 80, RW};
    RegisterHV2ModLUT2 = {"HV2ModLUT2", 81, RW};
    RegisterDCDCTLUT = {"DCDCTLUT", 82, RW};
    RegisterDCDCModLUT1 = {"DCDCModLUT1", 83, RW};
    RegisterDCDCModLUT2 = {"DCDCModLUT2", 84, RW};
    RegisterWRNTLUT = {"WRNTLUT", 85, RW};
    RegisterWRNModLUT1 = {"WRNModLUT1", 86, RW};
    RegisterWRNModLUT2 = {"WRNModLUT2", 87, RW};

    RegisterHxFlagEn = {"HxFlagEn", 90, RW};
    RegisterHyFlagEn = {"HyFlagEn", 91, RW};
    RegisterHV0FlagEn = {"HV0FlagEn", 92, RW};
    RegisterHV2FlagEn = {"HV2FlagEn", 93, RW};
    RegisterDCDCFlagEn = {"DCDCFlagEn", 94, RW};
    RegisterSynFlagEn = {"SynFlagEn", 95, RW};

    RegisterHxLoTh0 = {"HxLoTh0", 100, RW};
    RegisterHxLoTh1 = {"HxLoTh1", 101, RW};
    RegisterHxLoTh2 = {"HxLoTh2", 102, RW};
    RegisterHxLoTh3 = {"HxLoTh3", 103, RW};
    RegisterHxLoTh4 = {"HxLoTh4", 104, RW};
    RegisterHxLoTh5 = {"HxLoTh5", 105, RW};
    RegisterHxHiTh0 = {"HxHiTh0", 106, RW};
    RegisterHxHiTh1 = {"HxHiTh1", 107, RW};
    RegisterHxHiTh2 = {"HxHiTh2", 108, RW};
    RegisterHxHiTh3 = {"HxHiTh3", 109, RW};
    RegisterHxHiTh4 = {"HxHiTh4", 110, RW};
    RegisterHxHiTh5 = {"HxHiTh5", 111, RW};
    RegisterHyLoTh0 = {"HyLoTh0", 112, RW};
    RegisterHyLoTh1 = {"HyLoTh1", 113, RW};
    RegisterHyLoTh2 = {"HyLoTh2", 114, RW};
    RegisterHyLoTh3 = {"HyLoTh3", 115, RW};
    RegisterHyLoTh4 = {"HyLoTh4", 116, RW};
    RegisterHyLoTh5 = {"HyLoTh5", 117, RW};
    RegisterHyHiTh0 = {"HyHiTh0", 118, RW};
    RegisterHyHiTh1 = {"HyHiTh1", 119, RW};
    RegisterHyHiTh2 = {"HyHiTh2", 120, RW};
    RegisterHyHiTh3 = {"HyHiTh3", 121, RW};
    RegisterHyHiTh4 = {"HyHiTh4", 122, RW};
    RegisterHyHiTh5 = {"HyHiTh5", 123, RW};
    RegisterHV0LoTh0 = {"HV0LoTh0", 124, RW};
    RegisterHV0LoTh1 = {"HV0LoTh1", 125, RW};
    RegisterHV0LoTh2 = {"HV0LoTh2", 126, RW};
    RegisterHV0LoTh3 = {"HV0LoTh3", 127, RW};
    RegisterHV0LoTh4 = {"HV0LoTh4", 128, RW};
    RegisterHV0LoTh5 = {"HV0LoTh5", 129, RW};
    RegisterHV0HiTh0 = {"HV0HiTh0", 130, RW};
    RegisterHV0HiTh1 = {"HV0HiTh1", 131, RW};
    RegisterHV0HiTh2 = {"HV0HiTh2", 132, RW};
    RegisterHV0HiTh3 = {"HV0HiTh3", 133, RW};
    RegisterHV0HiTh4 = {"HV0HiTh4", 134, RW};
    RegisterHV0HiTh5 = {"HV0HiTh5", 135, RW};
    RegisterHV2LoTh0 = {"HV2LoTh0", 136, RW};
    RegisterHV2LoTh1 = {"HV2LoTh1", 137, RW};
    RegisterHV2LoTh2 = {"HV2LoTh2", 138, RW};
    RegisterHV2LoTh3 = {"HV2LoTh3", 139, RW};
    RegisterHV2LoTh4 = {"HV2LoTh4", 140, RW};
    RegisterHV2LoTh5 = {"HV2LoTh5", 141, RW};
    RegisterHV2HiTh0 = {"HV2HiTh0", 142, RW};
    RegisterHV2HiTh1 = {"HV2HiTh1", 143, RW};
    RegisterHV2HiTh2 = {"HV2HiTh2", 144, RW};
    RegisterHV2HiTh3 = {"HV2HiTh3", 145, RW};
    RegisterHV2HiTh4 = {"HV2HiTh4", 146, RW};
    RegisterHV2HiTh5 = {"HV2HiTh5", 147, RW};
    RegisterDCDCLoTh0 = {"DCDCLoTh0", 148, RW};
    RegisterDCDCLoTh1 = {"DCDCLoTh1", 149, RW};
    RegisterDCDCLoTh2 = {"DCDCLoTh2", 150, RW};
    RegisterDCDCLoTh3 = {"DCDCLoTh3", 151, RW};
    RegisterDCDCLoTh4 = {"DCDCLoTh4", 152, RW};
    RegisterDCDCLoTh5 = {"DCDCLoTh5", 153, RW};
    RegisterDCDCHiTh0 = {"DCDCHiTh0", 154, RW};
    RegisterDCDCHiTh1 = {"DCDCHiTh1", 155, RW};
    RegisterDCDCHiTh2 = {"DCDCHiTh2", 156, RW};
    RegisterDCDCHiTh3 = {"DCDCHiTh3", 157, RW};
    RegisterDCDCHiTh4 = {"DCDCHiTh4", 158, RW};
    RegisterDCDCHiTh5 = {"DCDCHiTh5", 159, RW};
    RegisterWRNLoTh0 = {"WRNLoTh0", 160, RW};
    RegisterWRNLoTh1 = {"WRNLoTh1", 161, RW};
    RegisterWRNLoTh2 = {"WRNLoTh2", 162, RW};
    RegisterWRNLoTh3 = {"WRNLoTh3", 163, RW};
    RegisterWRNLoTh4 = {"WRNLoTh4", 164, RW};
    RegisterWRNLoTh5 = {"WRNLoTh5", 165, RW};
    RegisterWRNHiTh0 = {"WRNHiTh0", 166, RW};
    RegisterWRNHiTh1 = {"WRNHiTh1", 167, RW};
    RegisterWRNHiTh2 = {"WRNHiTh2", 168, RW};
    RegisterWRNHiTh3 = {"WRNHiTh3", 169, RW};
    RegisterWRNHiTh4 = {"WRNHiTh4", 170, RW};
    RegisterWRNHiTh5 = {"WRNHiTh5", 171, RW};

    /** @} */

    /** \name Fields
     * @{ */
    // 0 - Status register
    StatusAM = {"StatusAM", &RegisterStatus, 31, 1, 0};
    StatusWARN = {"StatusWARN", &RegisterStatus, 29, 1, 0};
    StatusDCDC = {"StatusDCDC", &RegisterStatus, 28, 1, 0};
    StatusHV3 = {"StatusHV3", &RegisterStatus, 27, 1, 0};
    StatusHV2 = {"StatusHV2", &RegisterStatus, 26, 1, 0};
    StatusHV1 = {"StatusHV1", &RegisterStatus, 25, 1, 0};
    StatusHV0 = {"StatusHV0", &RegisterStatus, 24, 1, 0};
    StatusY2LDO = {"StatusY2LDO", &RegisterStatus, 22, 1, 0};
    StatusY1LDO = {"StatusY1LDO", &RegisterStatus, 21, 1, 0};
    StatusY0LDO = {"StatusY0LDO", &RegisterStatus, 20, 1, 0};
    StatusX2LDO = {"StatusX2LDO", &RegisterStatus, 18, 1, 0};
    StatusX1LDO = {"StatusX1LDO", &RegisterStatus, 17, 1, 0};
    StatusX0LDO = {"StatusX0LDO", &RegisterStatus, 16, 1, 0};
    StatusGPI = {"StatusGPI", &RegisterStatus, 12, 1, 0};
    StatusPGOOD = {"StatusPGOOD", &RegisterStatus, 8, 1, 0};
    StatusILockWARN = {"StatusILockWARN", &RegisterStatus, 5, 1, 0};
    StatusILockDCDC = {"StatusILockDCDC", &RegisterStatus, 4, 1, 0};
    StatusILockHV2 = {"StatusILockHV2", &RegisterStatus, 3, 1, 0};
    StatusILockHV0 = {"StatusILockHV2", &RegisterStatus, 2, 1, 0};
    StatusILockYLDO = {"StatusILockYLDO", &RegisterStatus, 1, 1, 0};
    StatusILockxLDO = {"StatusILockxLDO", &RegisterStatus, 0, 1, 0};
    AMACVersion = {"AMACVersion", &RegisterStatus, 6, 2, 0};
    // 1 - HxFlags
    HxFlagsHi = {"HxFlagsHi", &RegisterHxFlags, 16, 16, 0x0};
    HxFlagsLo = {"HxFlagsLo", &RegisterHxFlags, 0, 16, 0x0};
    // 2 - HyFlags
    HyFlagsHi = {"HyFlagsHi", &RegisterHyFlags, 16, 16, 0x0};
    HyFlagsLo = {"HyFlagsLo", &RegisterHyFlags, 0, 16, 0x0};
    // 3 - HV0Flags
    HV0FlagsHi = {"HV0FlagsHi", &RegisterHV0Flags, 16, 16, 0x0};
    HV0FlagsLo = {"HV0FlagsLo", &RegisterHV0Flags, 0, 16, 0x0};
    // 4 - HV2Flags
    HV2FlagsHi = {"HV2FlagsHi", &RegisterHV2Flags, 16, 16, 0x0};
    HV2FlagsLo = {"HV2FlagsLo", &RegisterHV2Flags, 0, 16, 0x0};
    // 5 - DCDCFlags
    DCDCflagsHi = {"DCDCflagsHi", &RegisterDCDCFlags, 16, 16, 0x0};
    DCDCflagsLo = {"DCDCflagsLo", &RegisterDCDCFlags, 0, 16, 0x0};
    // 6 - WRNHxFlags
    WRNflagsHi = {"WRNflagsHi", &RegisterWRNHxFlags, 16, 16, 0x0};
    WRNflagsLo = {"WRNflagsLo", &RegisterWRNHxFlags, 0, 16, 0x0};
    // 7 - SynFlags
    SynFlagsHx = {"SynFlagsHx", &RegisterSynFlags, 0, 2, 0x0};
    SynFlagsHy = {"SynFlagsHy", &RegisterSynFlags, 4, 2, 0x0};
    SynFlagsHV0 = {"SynFlagsHV0", &RegisterSynFlags, 8, 2, 0x0};
    SynFlagsHV2 = {"SynFlagsHV2", &RegisterSynFlags, 12, 2, 0x0};
    SynFlagsDCDC = {"SynFlagsDCDC", &RegisterSynFlags, 16, 2, 0x0};
    SynFlagsWRN = {"SynFlagsWRN", &RegisterSynFlags, 20, 2, 0x0};
    // 10 - Value0
    Value0AMen = {"Value0AMen", &RegisterValue0, 31, 1, 0x0};
    Ch0Value = {"Ch0Value", &RegisterValue0, 0, 10, 0x0};
    Ch1Value = {"Ch1Value", &RegisterValue0, 10, 10, 0x0};
    Ch2Value = {"Ch2Value", &RegisterValue0, 20, 10, 0x0};
    // 11 - Value1
    Value1AMen = {"Value1AMen", &RegisterValue1, 31, 1, 0x0};
    Ch3Value = {"Ch3Value", &RegisterValue1, 0, 10, 0x0};
    Ch4Value = {"Ch4Value", &RegisterValue1, 10, 10, 0x0};
    Ch5Value = {"Ch5Value", &RegisterValue1, 20, 10, 0x0};
    // 12 - Value2
    Value2AMen = {"Value2AMen", &RegisterValue2, 31, 1, 0x0};
    Ch6Value = {"Ch6Value", &RegisterValue2, 0, 10, 0x0};
    Ch7Value = {"Ch7Value", &RegisterValue2, 10, 10, 0x0};
    Ch8Value = {"Ch8Value", &RegisterValue2, 20, 10, 0x0};
    // 13 - Value3
    Value3AMen = {"Value3AMen", &RegisterValue3, 31, 1, 0x0};
    Ch9Value = {"Ch9Value", &RegisterValue3, 0, 10, 0x0};
    Ch10Value = {"Ch10Value", &RegisterValue3, 10, 10, 0x0};
    Ch11Value = {"Ch11Value", &RegisterValue3, 20, 10, 0x0};
    // 14 - Value4
    Value4AMen = {"Value4AMen", &RegisterValue4, 31, 1, 0x0};
    Ch12Value = {"Ch12Value", &RegisterValue4, 0, 10, 0x0};
    Ch13Value = {"Ch13Value", &RegisterValue4, 10, 10, 0x0};
    Ch14Value = {"Ch14Value", &RegisterValue4, 20, 10, 0x0};
    // 15 - Value5
    Value5AMen = {"Value5AMen", &RegisterValue5, 31, 1, 0x0};
    Ch15Value = {"Ch15Value", &RegisterValue5, 0, 10, 0x0};
    // 31 - SerNum
    PadID = {"PadID", &RegisterSerNum, 24, 5, 0x0};
    // 32 - FlagResets
    FlagResetWRN = {"FlagResetWRN", &RegisterFlagResets, 5, 1, 0x0};
    FlagResetDCDC = {"FlagResetDCDC", &RegisterFlagResets, 4, 1, 0x0};
    FlagResetHV2 = {"FlagResetHV2", &RegisterFlagResets, 3, 1, 0x0};
    FlagResetHV0 = {"FlagResetHV0", &RegisterFlagResets, 2, 1, 0x0};
    FlagResetXLDO = {"FlagResetXLDO", &RegisterFlagResets, 1, 1, 0x0};
    FlagResetYLDO = {"FlagResetYLDO", &RegisterFlagResets, 0, 1, 0x0};
    // 33 - LogicReset
    LogicReset = {"LogicReset", &RegisterLogicReset, 0, 32, 0x0};
    // 34 - HardReset
    HardReset = {"HardReset", &RegisterHardReset, 0, 32, 0x0};
    // 40 - CntSet
    CntSetHV3frq = {"CntSetHV3frq", &RegisterCntSet, 29, 2, 0x3};
    CntSetHV3en = {"CntSetHV3en", &RegisterCntSet, 28, 1, 0x0};
    CntSetHV2frq = {"CntSetHV2frq", &RegisterCntSet, 25, 2, 0x3};
    CntSetHV2en = {"CntSetHV2en", &RegisterCntSet, 24, 1, 0x0};
    CntSetHV1frq = {"CntSetHV1frq", &RegisterCntSet, 21, 2, 0x3};
    CntSetHV1en = {"CntSetHV1en", &RegisterCntSet, 20, 1, 0x0};
    CntSetHV0frq = {"CntSetHV0frq", &RegisterCntSet, 17, 2, 0x3};
    CntSetHV0en = {"CntSetHV0en", &RegisterCntSet, 16, 1, 0x0};
    CntSetHyLDO2en = {"CntSetHyLDO2en", &RegisterCntSet, 14, 1, 0x0};
    CntSetHyLDO1en = {"CntSetHyLDO1en", &RegisterCntSet, 13, 1, 0x0};
    CntSetHyLDO0en = {"CntSetHyLDO0en", &RegisterCntSet, 12, 1, 0x0};
    CntSetHxLDO2en = {"CntSetHxLDO2en", &RegisterCntSet, 10, 1, 0x0};
    CntSetHxLDO1en = {"CntSetHxLDO1en", &RegisterCntSet, 9, 1, 0x0};
    CntSetHxLDO0en = {"CntSetHxLDO0en", &RegisterCntSet, 8, 1, 0x0};
    CntSetWARN = {"CntSetWARN", &RegisterCntSet, 4, 1, 0x0};
    // 41 - CntSetC
    CntSetCHV3frq = {"CntSetCHV3frq", &RegisterCntSetC, 29, 2, 0x3};
    CntSetCHV3en = {"CntSetCHV3en", &RegisterCntSetC, 28, 1, 0x0};
    CntSetCHV2frq = {"CntSetCHV2frq", &RegisterCntSetC, 25, 2, 0x3};
    CntSetCHV2en = {"CntSetCHV2en", &RegisterCntSetC, 24, 1, 0x0};
    CntSetCHV1frq = {"CntSetCHV1frq", &RegisterCntSetC, 21, 2, 0x3};
    CntSetCHV1en = {"CntSetCHV1en", &RegisterCntSetC, 20, 1, 0x0};
    CntSetCHV0frq = {"CntSetCHV0frq", &RegisterCntSetC, 17, 2, 0x3};
    CntSetCHV0en = {"CntSetCHV0en", &RegisterCntSetC, 16, 1, 0x0};
    CntSetCHyLDO2en = {"CntSetCHyLDO2en", &RegisterCntSetC, 14, 1, 0x0};
    CntSetCHyLDO1en = {"CntSetCHyLDO1en", &RegisterCntSetC, 13, 1, 0x0};
    CntSetCHyLDO0en = {"CntSetCHyLDO0en", &RegisterCntSetC, 12, 1, 0x0};
    CntSetCHxLDO2en = {"CntSetCHxLDO2en", &RegisterCntSetC, 10, 1, 0x0};
    CntSetCHxLDO1en = {"CntSetCHxLDO1en", &RegisterCntSetC, 9, 1, 0x0};
    CntSetCHxLDO0en = {"CntSetCHxLDO0en", &RegisterCntSetC, 8, 1, 0x0};
    CntSetCWARN = {"CntSetCWARN", &RegisterCntSetC, 4, 1, 0x0};
    // 42 - DCDCen
    DCDCAdj = {"DCDCAdj", &RegisterDCDCen, 4, 2, 0x0};
    DCDCen = {"DCDCen", &RegisterDCDCen, 0, 1, 0x0};
    // 43 - DCDCenC
    DCDCAdjC = {"DCDCAdjC", &RegisterDCDCenC, 4, 2, 0x0};
    DCDCenC = {"DCDCenC", &RegisterDCDCenC, 0, 1, 0x0};
    // 44 - Ilock
    IlockHx = {"IlockHx", &RegisterIlock, 0, 1, 0x0};
    IlockHy = {"IlockHy", &RegisterIlock, 1, 1, 0x0};
    IlockHV0 = {"IlockHV0", &RegisterIlock, 2, 1, 0x0};
    IlockHV2 = {"IlockHV2", &RegisterIlock, 3, 1, 0x0};
    IlockDCDC = {"IlockDCDC", &RegisterIlock, 4, 1, 0x0};
    IlockWRN = {"IlockWRN", &RegisterIlock, 5, 1, 0x0};
    // 45 - IlockC
    IlockCHx = {"IlockCHx", &RegisterIlockC, 0, 1, 0x0};
    IlockCHy = {"IlockCHy", &RegisterIlockC, 1, 1, 0x0};
    IlockCHV0 = {"IlockCHV0", &RegisterIlockC, 2, 1, 0x0};
    IlockCHV2 = {"IlockCHV2", &RegisterIlockC, 3, 1, 0x0};
    IlockCDCDC = {"IlockCDCDC", &RegisterIlockC, 4, 1, 0x0};
    IlockCWRN = {"IlockCWRN", &RegisterIlockC, 5, 1, 0x0};
    // 46 - RstCnt
    RstCntHyHCCresetB = {"RstCntHyHCCresetB", &RegisterRstCnt, 16, 1, 0x0};
    RstCntHxHCCresetB = {"RstCntHxHCCresetB", &RegisterRstCnt, 8, 1, 0x0};
    RstCntOF = {"RstCntOF", &RegisterRstCnt, 0, 1, 0x0};
    // 47 - RstCntC
    RstCntCHyHCCresetB = {"RstCntCHyHCCresetB", &RegisterRstCntC, 16, 1, 0x0};
    RstCntCHxHCCresetB = {"RstCntCHxHCCresetB", &RegisterRstCntC, 8, 1, 0x0};
    RstCntCOF = {"RstCntCOF", &RegisterRstCntC, 0, 1, 0x0};
    // 48 - AMen
    AMzeroCalib = {"AMzeroCalib", &RegisterAMen, 8, 1, 0x0};
    AMen = {"AMen", &RegisterAMen, 0, 1, 0x1};
    // 49 - AMenC
    AMzeroCalibC = {"AMzeroCalibC", &RegisterAMenC, 8, 1, 0x0};
    AMenC = {"AMenC", &RegisterAMenC, 0, 1, 0x1};
    // 50 - AMpwr
    ReqDCDCPGOOD = {"ReqDCDCPGOOD", &RegisterAMpwr, 8, 1, 0x1};
    DCDCenToPwrAMAC = {"DCDCenToPwrAMAC", &RegisterAMpwr, 0, 1, 0x0};
    // 51 - AMpwrC
    ReqDCDCPGOODC = {"ReqDCDCPGOODC", &RegisterAMpwrC, 8, 1, 0x1};
    DCDCenToPwrAMACC = {"DCDCenToPwrAMACC", &RegisterAMpwrC, 0, 1, 0x0};
    // 52 - BgCnt
    AMbgen = {"AMbgen", &RegisterBgCnt, 15, 1, 0x0};
    AMbg = {"AMbg", &RegisterBgCnt, 8, 5, 0x0};
    VDDbgen = {"VDDbgen", &RegisterBgCnt, 7, 1, 0x0};
    VDDbg = {"VDDbg", &RegisterBgCnt, 0, 5, 0x0};
    // 53 - AMCnt
    AMintCalib = {"AMintCalib", &RegisterAMCnt, 24, 4, 0x0};
    Ch13Mux = {"Ch13Mux", &RegisterAMCnt, 20, 2, 0x0};
    Ch12Mux = {"Ch12Mux", &RegisterAMCnt, 16, 2, 0x0};
    Ch5Mux = {"Ch5Mux", &RegisterAMCnt, 12, 2, 0x0};
    Ch4Mux = {"Ch4Mux", &RegisterAMCnt, 8, 2, 0x0};
    Ch3Mux = {"Ch3Mux", &RegisterAMCnt, 4, 2, 0x0};
    // 54 - DACs0
    DACShunty = {"DACShunty", &RegisterDACs0, 24, 8, 0x0};
    DACShuntx = {"DACShuntx", &RegisterDACs0, 16, 8, 0x0};
    DACCaly = {"DACCaly", &RegisterDACs0, 8, 8, 0x0};
    DACCalx = {"DACCalx", &RegisterDACs0, 0, 8, 0x0};
    // 55 - DACbias
    DACbias = {"DACbias", &RegisterDACbias, 0, 5, 0xD};
    // 56 - AMACCnt
    HVcurGain = {"HVcurGain", &RegisterAMACCnt, 16, 4, 0x0};
    DRcomMode = {"DRcomMode", &RegisterAMACCnt, 12, 2, 0x0};
    DRcurr = {"DRcurr", &RegisterAMACCnt, 8, 3, 0x4};
    RingOscFrq = {"RingOscFrq", &RegisterAMACCnt, 0, 3, 0x4};
    // 57 - NTCRange
    CTAToffset = {"CTAToffset", &RegisterNTCRange, 20, 4, 0x0};
    NTCpbCal = {"NTCpbCal", &RegisterNTCRange, 19, 1, 0x1};
    NTCpbSenseRange = {"NTCpbSenseRange", &RegisterNTCRange, 16, 3, 0x4};
    NTCy0Cal = {"NTCy0Cal", &RegisterNTCRange, 11, 1, 0x1};
    NTCy0SenseRange = {"NTCy0SenseRange", &RegisterNTCRange, 8, 3, 0x4};
    NTCx0Cal = {"NTCx0Cal", &RegisterNTCRange, 3, 1, 0x1};
    NTCx0SenseRange = {"NTCx0SenseRange", &RegisterNTCRange, 0, 3, 0x4};
    // 58 - LVCurCal
    DCDCoOffset = {"DCDCoOffset", &RegisterLVCurCal, 20, 4, 0x4};
    DCDCoZeroReading = {"DCDCoZeroReading", &RegisterLVCurCal, 19, 1, 0x0};
    DCDCoN = {"DCDCoN", &RegisterLVCurCal, 17, 1, 0x0};
    DCDCoP = {"DCDCoP", &RegisterLVCurCal, 16, 1, 0x0};
    DCDCiZeroReading = {"DCDCiZeroReading", &RegisterLVCurCal, 15, 1, 0x0};
    DCDCiRangeSW = {"DCDCiRangeSW", &RegisterLVCurCal, 12, 1, 0x1};
    DCDCiOffset = {"DCDCiOffset", &RegisterLVCurCal, 8, 4, 0x8};
    DCDCiP = {"DCDCiP", &RegisterLVCurCal, 4, 3, 0x0};
    DCDCiN = {"DCDCiN", &RegisterLVCurCal, 0, 3, 0x0};
    // 60 - HxICfg
    HxFlagValid = {"HxFlagValid", &RegisterHxICfg, 0, 2, 0x0};
    HxFlagValidEn = {"HxFlagValidEn", &RegisterHxICfg, 4, 1, 0x0};
    HxFlagsLogic = {"HxFlagsLogic", &RegisterHxICfg, 8, 1, 0x0};
    HxFlagsLatch = {"HxFlagsLatch", &RegisterHxICfg, 12, 1, 0x1};
    HxLAM = {"HxLAM", &RegisterHxICfg, 16, 1, 0x0};
    // 61 - HyICfg
    HyFlagValid = {"HyFlagValid", &RegisterHyICfg, 0, 2, 0x0};
    HyFlagValidEn = {"HyFlagValidEn", &RegisterHyICfg, 4, 1, 0x0};
    HyFlagsLogic = {"HyFlagsLogic", &RegisterHyICfg, 8, 1, 0x0};
    HyFlagsLatch = {"HyFlagsLatch", &RegisterHyICfg, 12, 1, 0x1};
    HyLAM = {"HyLAM", &RegisterHyICfg, 16, 1, 0x0};
    // 62 - HV0ICfg
    HV0FlagValid = {"HV0FlagValid", &RegisterHV0ICfg, 0, 2, 0x0};
    HV0FlagValidEn = {"HV0FlagValidEn", &RegisterHV0ICfg, 4, 1, 0x0};
    HV0FlagsLogic = {"HV0FlagsLogic", &RegisterHV0ICfg, 8, 1, 0x0};
    HV0FlagsLatch = {"HV0FlagsLatch", &RegisterHV0ICfg, 12, 1, 0x1};
    HV0LAM = {"HV0LAM", &RegisterHV0ICfg, 16, 1, 0x0};
    // 63 - HV2ICfg
    HV2FlagValid = {"HV2FlagValid", &RegisterHV2ICfg, 0, 2, 0x0};
    HV2FlagValidEn = {"HV2FlagValidEn", &RegisterHV2ICfg, 4, 1, 0x0};
    HV2FlagsLogic = {"HV2FlagsLogic", &RegisterHV2ICfg, 8, 1, 0x0};
    HV2FlagsLatch = {"HV2FlagsLatch", &RegisterHV2ICfg, 12, 1, 0x1};
    HV2LAM = {"HV2LAM", &RegisterHV2ICfg, 16, 1, 0x0};
    // 64 - DCDCICfg
    DCDCFlagValid = {"DCDCFlagValid", &RegisterDCDCICfg, 0, 2, 0x0};
    DCDCFlagValidEn = {"DCDCFlagValidEn", &RegisterDCDCICfg, 4, 1, 0x0};
    DCDCFlagsLogic = {"DCDCFlagsLogic", &RegisterDCDCICfg, 8, 1, 0x0};
    DCDCFlagsLatch = {"DCDCFlagsLatch", &RegisterDCDCICfg, 12, 1, 0x1};
    DCDCLAM = {"DCDCLAM", &RegisterDCDCICfg, 16, 1, 0x0};
    // 65 - WRNICfg
    WRNFlagValid = {"WRNFlagValid", &RegisterWRNICfg, 0, 2, 0x0};
    WRNFlagValidEn = {"WRNFlagValidEn", &RegisterWRNICfg, 4, 1, 0x0};
    WRNFlagsLogic = {"WRNFlagsLogic", &RegisterWRNICfg, 8, 1, 0x0};
    WRNFlagsLatch = {"WRNFlagsLatch", &RegisterWRNICfg, 12, 1, 0x1};
    WRNLAM = {"WRNLAM", &RegisterWRNICfg, 16, 1, 0x0};
    // 70 - HxTLUT
    HxTlut = {"HxTlut", &RegisterHxTLUT, 0, 8, 0x0};
    // 71 - HxModLUT1
    HxModlut1 = {"HxModlut1", &RegisterHxModLUT1, 0, 32, 0x0};
    // 72 - HxModLUT2
    HxModlut2 = {"HxModlut2", &RegisterHxModLUT2, 0, 32, 0x0};
    // 73 - HyTLUT
    HyTlut = {"HyTlut", &RegisterHyTLUT, 0, 8, 0x0};
    // 74 - HyModLUT1
    HyModlut1 = {"HyModlut1", &RegisterHyModLUT1, 0, 32, 0x0};
    // 75 - HyModLUT2
    HyModlut2 = {"HyModlut2", &RegisterHyModLUT2, 0, 32, 0x0};
    // 76 - HV0TLUT
    HV0Tlut = {"HV0Tlut", &RegisterHV0TLUT, 0, 8, 0x0};
    // 77 - HV0ModLUT1
    HV0Modlut1 = {"HV0Modlut1", &RegisterHV0ModLUT1, 0, 32, 0x0};
    // 78 - HV0ModLUT2
    HV0Modlut2 = {"HV0Modlut2", &RegisterHV0ModLUT2, 0, 32, 0x0};
    // 79 - HV2TLUT
    HV2Tlut = {"HV2Tlut", &RegisterHV2TLUT, 0, 8, 0x0};
    // 80 - HV2ModLUT1
    HV2Modlut1 = {"HV2Modlut1", &RegisterHV2ModLUT1, 0, 32, 0x0};
    // 81 - HV2ModLUT2
    HV2Modlut2 = {"HV2Modlut2", &RegisterHV2ModLUT2, 0, 32, 0x0};
    // 82 - DCDCTLUT
    DCDCTlut = {"DCDCTlut", &RegisterDCDCTLUT, 0, 8, 0x0};
    // 83 - DCDCModLUT1
    DCDCModlut1 = {"DCDCModlut1", &RegisterDCDCModLUT1, 0, 32, 0x0};
    // 84 - DCDCModLUT2
    DCDCModlut2 = {"DCDCModlut2", &RegisterDCDCModLUT2, 0, 32, 0x0};
    // 85 - WRNTLUT
    WRNTlut = {"WRNTlut", &RegisterWRNTLUT, 0, 8, 0x0};
    // 86 - WRNModLUT1
    WRNModlut1 = {"WRNModlut1", &RegisterWRNModLUT1, 0, 32, 0x0};
    // 87 - WRNModLUT2
    WRNModlut2 = {"WRNModlut2", &RegisterWRNModLUT2, 0, 32, 0x0};
    // 90 - HxFlagEn
    HxFlagsEnHi = {"HxFlagsEnHi", &RegisterHxFlagEn, 16, 16, 0x0};
    HxFlagsEnLo = {"HxFlagsEnLo", &RegisterHxFlagEn, 16, 16, 0x0};
    // 91 - HyFlagEn
    HyFlagsEnHi = {"HyFlagsEnHi", &RegisterHyFlagEn, 16, 16, 0x0};
    HyFlagsEnLo = {"HyFlagsEnLo", &RegisterHyFlagEn, 16, 16, 0x0};
    // 92 - HV0FlagEn
    HV0FlagsEnHi = {"HV0FlagsEnHi", &RegisterHV0FlagEn, 16, 16, 0x0};
    HV0FlagsEnLo = {"HV0FlagsEnLo", &RegisterHV0FlagEn, 16, 16, 0x0};
    // 93 - HV2FlagEn
    HV2FlagsEnHi = {"HV2FlagsEnHi", &RegisterHV2FlagEn, 16, 16, 0x0};
    HV2FlagsEnLo = {"HV2FlagsEnLo", &RegisterHV2FlagEn, 16, 16, 0x0};
    // 94 - DCDCFlagEn
    DCDCFlagsEnHi = {"DCDCFlagsEnHi", &RegisterDCDCFlagEn, 16, 16, 0x0};
    DCDCFlagsEnLo = {"DCDCFlagsEnLo", &RegisterDCDCFlagEn, 16, 16, 0x0};
    // 95 - SynFlagEn
    WRNFlagsEnHi = {"WRNFlagsEnHi", &RegisterSynFlagEn, 16, 16, 0x0};
    WRNFlagsEnLo = {"WRNFlagsEnLo", &RegisterSynFlagEn, 16, 16, 0x0};
    WRNsynFlagEnHi = {"WRNsynFlagEnHi", &RegisterSynFlagEn, 21, 1, 0x0};
    WRNsynFlagEnLo = {"WRNsynFlagEnLo", &RegisterSynFlagEn, 20, 1, 0x0};
    DCDCsynFlagEnHi = {"DCDCsynFlagEnHi", &RegisterSynFlagEn, 17, 1, 0x0};
    DCDCsynFlagEnLo = {"DCDCsynFlagEnLo", &RegisterSynFlagEn, 16, 1, 0x0};
    HV2synFlagEnHi = {"HV2synFlagEnHi", &RegisterSynFlagEn, 13, 1, 0x0};
    HV2synFlagEnLo = {"HV2synFlagEnLo", &RegisterSynFlagEn, 12, 1, 0x0};
    HV0synFlagEnHi = {"HV0synFlagEnHi", &RegisterSynFlagEn, 9, 1, 0x0};
    HV0synFlagEnLo = {"HV0synFlagEnLo", &RegisterSynFlagEn, 8, 1, 0x0};
    HysynFlagEnHi = {"HysynFlagEnHi", &RegisterSynFlagEn, 5, 1, 0x0};
    HysynFlagEnLo = {"HysynFlagEnLo", &RegisterSynFlagEn, 4, 1, 0x0};
    HxsynFlagEnHi = {"HxsynFlagEnHi", &RegisterSynFlagEn, 1, 1, 0x0};
    HxsynFlagEnLo = {"HxsynFlagEnLo", &RegisterSynFlagEn, 0, 1, 0x0};
    // 100 - HxLoTh0
    HxLoThCh0 = {"HxLoThCh0", &RegisterHxLoTh0, 0, 10, 0x0};
    HxLoThCh1 = {"HxLoThCh1", &RegisterHxLoTh0, 10, 10, 0x0};
    HxLoThCh2 = {"HxLoThCh2", &RegisterHxLoTh0, 20, 10, 0x0};
    // 101 - HxLoTh1
    HxLoThCh3 = {"HxLoThCh3", &RegisterHxLoTh1, 0, 10, 0x0};
    HxLoThCh4 = {"HxLoThCh4", &RegisterHxLoTh1, 10, 10, 0x0};
    HxLoThCh5 = {"HxLoThCh5", &RegisterHxLoTh1, 20, 10, 0x0};
    // 102 - HxLoTh2
    HxLoThCh6 = {"HxLoThCh6", &RegisterHxLoTh2, 0, 10, 0x0};
    HxLoThCh7 = {"HxLoThCh7", &RegisterHxLoTh2, 10, 10, 0x0};
    HxLoThCh8 = {"HxLoThCh8", &RegisterHxLoTh2, 20, 10, 0x0};
    // 103 - HxLoTh3
    HxLoThCh9 = {"HxLoThCh9", &RegisterHxLoTh3, 0, 10, 0x0};
    HxLoThCh10 = {"HxLoThCh10", &RegisterHxLoTh3, 10, 10, 0x0};
    HxLoThCh11 = {"HxLoThCh11", &RegisterHxLoTh3, 20, 10, 0x0};
    // 104 - HxLoTh4
    HxLoThCh12 = {"HxLoThCh12", &RegisterHxLoTh4, 0, 10, 0x0};
    HxLoThCh13 = {"HxLoThCh13", &RegisterHxLoTh4, 10, 10, 0x0};
    HxLoThCh14 = {"HxLoThCh14", &RegisterHxLoTh4, 20, 10, 0x0};
    // 105 - HxLoTh5
    HxLoThCh15 = {"HxLoThCh15", &RegisterHxLoTh5, 0, 10, 0x0};
    // 106 - HxHiTh0
    HxHiThCh0 = {"HxHiThCh0", &RegisterHxHiTh0, 0, 10, 0x3FF};
    HxHiThCh1 = {"HxHiThCh1", &RegisterHxHiTh0, 10, 10, 0x3FF};
    HxHiThCh2 = {"HxHiThCh2", &RegisterHxHiTh0, 20, 10, 0x3FF};
    // 107 - HxHiTh1
    HxHiThCh3 = {"HxHiThCh3", &RegisterHxHiTh1, 0, 10, 0x3FF};
    HxHiThCh4 = {"HxHiThCh4", &RegisterHxHiTh1, 10, 10, 0x3FF};
    HxHiThCh5 = {"HxHiThCh5", &RegisterHxHiTh1, 20, 10, 0x3FF};
    // 108 - HxHiTh2
    HxHiThCh6 = {"HxHiThCh6", &RegisterHxHiTh2, 0, 10, 0x3FF};
    HxHiThCh7 = {"HxHiThCh7", &RegisterHxHiTh2, 10, 10, 0x3FF};
    HxHiThCh8 = {"HxHiThCh8", &RegisterHxHiTh2, 20, 10, 0x3FF};
    // 109 - HxHiTh3
    HxHiThCh9 = {"HxHiThCh9", &RegisterHxHiTh3, 0, 10, 0x3FF};
    HxHiThCh10 = {"HxHiThCh10", &RegisterHxHiTh3, 10, 10, 0x3FF};
    HxHiThCh11 = {"HxHiThCh11", &RegisterHxHiTh3, 20, 10, 0x3FF};
    // 110 - HxHiTh4
    HxHiThCh12 = {"HxHiThCh12", &RegisterHxHiTh4, 0, 10, 0x3FF};
    HxHiThCh13 = {"HxHiThCh13", &RegisterHxHiTh4, 10, 10, 0x3FF};
    HxHiThCh14 = {"HxHiThCh14", &RegisterHxHiTh4, 20, 10, 0x3FF};
    // 111 - HxHiTh5
    HxHiThCh15 = {"HxHiThCh15", &RegisterHxHiTh5, 0, 10, 0x3FF};
    // 112 - HyLoTh0
    HyLoThCh0 = {"HyLoThCh0", &RegisterHyLoTh0, 0, 10, 0x0};
    HyLoThCh1 = {"HyLoThCh1", &RegisterHyLoTh0, 10, 10, 0x0};
    HyLoThCh2 = {"HyLoThCh2", &RegisterHyLoTh0, 20, 10, 0x0};
    // 113 - HyLoTh1
    HyLoThCh3 = {"HyLoThCh3", &RegisterHyLoTh1, 0, 10, 0x0};
    HyLoThCh4 = {"HyLoThCh4", &RegisterHyLoTh1, 10, 10, 0x0};
    HyLoThCh5 = {"HyLoThCh5", &RegisterHyLoTh1, 20, 10, 0x0};
    // 114 - HyLoTh2
    HyLoThCh6 = {"HyLoThCh6", &RegisterHyLoTh2, 0, 10, 0x0};
    HyLoThCh7 = {"HyLoThCh7", &RegisterHyLoTh2, 10, 10, 0x0};
    HyLoThCh8 = {"HyLoThCh8", &RegisterHyLoTh2, 20, 10, 0x0};
    // 115 - HyLoTh3
    HyLoThCh9 = {"HyLoThCh9", &RegisterHyLoTh3, 0, 10, 0x0};
    HyLoThCh10 = {"HyLoThCh10", &RegisterHyLoTh3, 10, 10, 0x0};
    HyLoThCh11 = {"HyLoThCh11", &RegisterHyLoTh3, 20, 10, 0x0};
    // 116 - HyLoTh4
    HyLoThCh12 = {"HyLoThCh12", &RegisterHyLoTh4, 0, 10, 0x0};
    HyLoThCh13 = {"HyLoThCh13", &RegisterHyLoTh4, 10, 10, 0x0};
    HyLoThCh14 = {"HyLoThCh14", &RegisterHyLoTh4, 20, 10, 0x0};
    // 117 - HyLoTh5
    HyLoThCh15 = {"HyLoThCh15", &RegisterHyLoTh5, 0, 10, 0x0};
    // 118 - HyHiTh0
    HyHiThCh0 = {"HyHiThCh0", &RegisterHyHiTh0, 0, 10, 0x3FF};
    HyHiThCh1 = {"HyHiThCh1", &RegisterHyHiTh0, 10, 10, 0x3FF};
    HyHiThCh2 = {"HyHiThCh2", &RegisterHyHiTh0, 20, 10, 0x3FF};
    // 119 - HyHiTh1
    HyHiThCh3 = {"HyHiThCh3", &RegisterHyHiTh1, 0, 10, 0x3FF};
    HyHiThCh4 = {"HyHiThCh4", &RegisterHyHiTh1, 10, 10, 0x3FF};
    HyHiThCh5 = {"HyHiThCh5", &RegisterHyHiTh1, 20, 10, 0x3FF};
    // 120 - HyHiTh2
    HyHiThCh6 = {"HyHiThCh6", &RegisterHyHiTh2, 0, 10, 0x3FF};
    HyHiThCh7 = {"HyHiThCh7", &RegisterHyHiTh2, 10, 10, 0x3FF};
    HyHiThCh8 = {"HyHiThCh8", &RegisterHyHiTh2, 20, 10, 0x3FF};
    // 121 - HyHiTh3
    HyHiThCh9 = {"HyHiThCh9", &RegisterHyHiTh3, 0, 10, 0x3FF};
    HyHiThCh10 = {"HyHiThCh10", &RegisterHyHiTh3, 10, 10, 0x3FF};
    HyHiThCh11 = {"HyHiThCh11", &RegisterHyHiTh3, 20, 10, 0x3FF};
    // 122 - HyHiTh4
    HyHiThCh12 = {"HyHiThCh12", &RegisterHyHiTh4, 0, 10, 0x3FF};
    HyHiThCh13 = {"HyHiThCh13", &RegisterHyHiTh4, 10, 10, 0x3FF};
    HyHiThCh14 = {"HyHiThCh14", &RegisterHyHiTh4, 20, 10, 0x3FF};
    // 123 - HyHiTh5
    HyHiThCh15 = {"HyHiThCh15", &RegisterHyHiTh5, 0, 10, 0x3FF};
    // 124 - HV0LoTh0
    HV0LoThCh0 = {"HV0LoThCh0", &RegisterHV0LoTh0, 0, 10, 0x0};
    HV0LoThCh1 = {"HV0LoThCh1", &RegisterHV0LoTh0, 10, 10, 0x0};
    HV0LoThCh2 = {"HV0LoThCh2", &RegisterHV0LoTh0, 20, 10, 0x0};
    // 125 - HV0LoTh1
    HV0LoThCh3 = {"HV0LoThCh3", &RegisterHV0LoTh1, 0, 10, 0x0};
    HV0LoThCh4 = {"HV0LoThCh4", &RegisterHV0LoTh1, 10, 10, 0x0};
    HV0LoThCh5 = {"HV0LoThCh5", &RegisterHV0LoTh1, 20, 10, 0x0};
    // 126 - HV0LoTh2
    HV0LoThCh6 = {"HV0LoThCh6", &RegisterHV0LoTh2, 0, 10, 0x0};
    HV0LoThCh7 = {"HV0LoThCh7", &RegisterHV0LoTh2, 10, 10, 0x0};
    HV0LoThCh8 = {"HV0LoThCh8", &RegisterHV0LoTh2, 20, 10, 0x0};
    // 127 - HV0LoTh3
    HV0LoThCh9 = {"HV0LoThCh9", &RegisterHV0LoTh3, 0, 10, 0x0};
    HV0LoThCh10 = {"HV0LoThCh10", &RegisterHV0LoTh3, 10, 10, 0x0};
    HV0LoThCh11 = {"HV0LoThCh11", &RegisterHV0LoTh3, 20, 10, 0x0};
    // 128 - HV0LoTh4
    HV0LoThCh12 = {"HV0LoThCh12", &RegisterHV0LoTh4, 0, 10, 0x0};
    HV0LoThCh13 = {"HV0LoThCh13", &RegisterHV0LoTh4, 10, 10, 0x0};
    HV0LoThCh14 = {"HV0LoThCh14", &RegisterHV0LoTh4, 20, 10, 0x0};
    // 129 - HV0LoTh5
    HV0LoThCh15 = {"HV0LoThCh15", &RegisterHV0LoTh5, 0, 10, 0x0};
    // 130 - HV0HiTh0
    HV0HiThCh0 = {"HV0HiThCh0", &RegisterHV0HiTh0, 0, 10, 0x3FF};
    HV0HiThCh1 = {"HV0HiThCh1", &RegisterHV0HiTh0, 10, 10, 0x3FF};
    HV0HiThCh2 = {"HV0HiThCh2", &RegisterHV0HiTh0, 20, 10, 0x3FF};
    // 131 - HV0HiTh1
    HV0HiThCh3 = {"HV0HiThCh3", &RegisterHV0HiTh1, 0, 10, 0x3FF};
    HV0HiThCh4 = {"HV0HiThCh4", &RegisterHV0HiTh1, 10, 10, 0x3FF};
    HV0HiThCh5 = {"HV0HiThCh5", &RegisterHV0HiTh1, 20, 10, 0x3FF};
    // 132 - HV0HiTh2
    HV0HiThCh6 = {"HV0HiThCh6", &RegisterHV0HiTh2, 0, 10, 0x3FF};
    HV0HiThCh7 = {"HV0HiThCh7", &RegisterHV0HiTh2, 10, 10, 0x3FF};
    HV0HiThCh8 = {"HV0HiThCh8", &RegisterHV0HiTh2, 20, 10, 0x3FF};
    // 133 - HV0HiTh3
    HV0HiThCh9 = {"HV0HiThCh9", &RegisterHV0HiTh3, 0, 10, 0x3FF};
    HV0HiThCh10 = {"HV0HiThCh10", &RegisterHV0HiTh3, 10, 10, 0x3FF};
    HV0HiThCh11 = {"HV0HiThCh11", &RegisterHV0HiTh3, 20, 10, 0x3FF};
    // 134 - HV0HiTh4
    HV0HiThCh12 = {"HV0HiThCh12", &RegisterHV0HiTh4, 0, 10, 0x3FF};
    HV0HiThCh13 = {"HV0HiThCh13", &RegisterHV0HiTh4, 10, 10, 0x3FF};
    HV0HiThCh14 = {"HV0HiThCh14", &RegisterHV0HiTh4, 20, 10, 0x3FF};
    // 135 - HV0HiTh5
    HV0HiThCh15 = {"HV0HiThCh15", &RegisterHV0HiTh5, 0, 10, 0x3FF};
    // 136 - HV2HiTh0
    HV2LoThCh0 = {"HV2LoThCh0", &RegisterHV2LoTh0, 0, 10, 0x0};
    HV2LoThCh1 = {"HV2LoThCh1", &RegisterHV2LoTh0, 10, 10, 0x0};
    HV2LoThCh2 = {"HV2LoThCh2", &RegisterHV2LoTh0, 20, 10, 0x0};
    // 137 - HV2LoTh1
    HV2LoThCh3 = {"HV2LoThCh3", &RegisterHV2LoTh1, 0, 10, 0x0};
    HV2LoThCh4 = {"HV2LoThCh4", &RegisterHV2LoTh1, 10, 10, 0x0};
    HV2LoThCh5 = {"HV2LoThCh5", &RegisterHV2LoTh1, 20, 10, 0x0};
    // 138 - HV2LoTh2
    HV2LoThCh6 = {"HV2LoThCh6", &RegisterHV2LoTh2, 0, 10, 0x0};
    HV2LoThCh7 = {"HV2LoThCh7", &RegisterHV2LoTh2, 10, 10, 0x0};
    HV2LoThCh8 = {"HV2LoThCh8", &RegisterHV2LoTh2, 20, 10, 0x0};
    // 139 - HV2LoTh3
    HV2LoThCh9 = {"HV2LoThCh9", &RegisterHV2LoTh3, 0, 10, 0x0};
    HV2LoThCh10 = {"HV2LoThCh10", &RegisterHV2LoTh3, 10, 10, 0x0};
    HV2LoThCh11 = {"HV2LoThCh11", &RegisterHV2LoTh3, 20, 10, 0x0};
    // 140 - HV2LoTh4
    HV2LoThCh12 = {"HV2LoThCh12", &RegisterHV2LoTh4, 0, 10, 0x0};
    HV2LoThCh13 = {"HV2LoThCh13", &RegisterHV2LoTh4, 10, 10, 0x0};
    HV2LoThCh14 = {"HV2LoThCh14", &RegisterHV2LoTh4, 20, 10, 0x0};
    // 141 - HV2LoTh5
    HV2LoThCh15 = {"HV2LoThCh15", &RegisterHV2LoTh5, 0, 10, 0x0};
    // 142 - HV2HiTh0
    HV2HiThCh0 = {"HV2HiThCh0", &RegisterHV2HiTh0, 0, 10, 0x3FF};
    HV2HiThCh1 = {"HV2HiThCh1", &RegisterHV2HiTh0, 10, 10, 0x3FF};
    HV2HiThCh2 = {"HV2HiThCh2", &RegisterHV2HiTh0, 20, 10, 0x3FF};
    // 143 - HV2HiTh1
    HV2HiThCh3 = {"HV2HiThCh3", &RegisterHV2HiTh1, 0, 10, 0x3FF};
    HV2HiThCh4 = {"HV2HiThCh4", &RegisterHV2HiTh1, 10, 10, 0x3FF};
    HV2HiThCh5 = {"HV2HiThCh5", &RegisterHV2HiTh1, 20, 10, 0x3FF};
    // 144 - HV2HiTh2
    HV2HiThCh6 = {"HV2HiThCh6", &RegisterHV2HiTh2, 0, 10, 0x3FF};
    HV2HiThCh7 = {"HV2HiThCh7", &RegisterHV2HiTh2, 10, 10, 0x3FF};
    HV2HiThCh8 = {"HV2HiThCh8", &RegisterHV2HiTh2, 20, 10, 0x3FF};
    // 145 - HV2HiTh3
    HV2HiThCh9 = {"HV2HiThCh9", &RegisterHV2HiTh3, 0, 10, 0x3FF};
    HV2HiThCh10 = {"HV2HiThCh10", &RegisterHV2HiTh3, 10, 10, 0x3FF};
    HV2HiThCh11 = {"HV2HiThCh11", &RegisterHV2HiTh3, 20, 10, 0x3FF};
    // 146 - HV2HiTh4
    HV2HiThCh12 = {"HV2HiThCh12", &RegisterHV2HiTh4, 0, 10, 0x3FF};
    HV2HiThCh13 = {"HV2HiThCh13", &RegisterHV2HiTh4, 10, 10, 0x3FF};
    HV2HiThCh14 = {"HV2HiThCh14", &RegisterHV2HiTh4, 20, 10, 0x3FF};
    // 147 - HV2HiTh5
    HV2HiThCh15 = {"HV2HiThCh15", &RegisterHV2HiTh5, 0, 10, 0x3FF};
    // 148 - DCDCLoTh0
    DCDCLoThCh0 = {"DCDCLoThCh0", &RegisterDCDCLoTh0, 0, 10, 0x0};
    DCDCLoThCh1 = {"DCDCLoThCh1", &RegisterDCDCLoTh0, 10, 10, 0x0};
    DCDCLoThCh2 = {"DCDCLoThCh2", &RegisterDCDCLoTh0, 20, 10, 0x0};
    // 149 - DCDCLoTh1
    DCDCLoThCh3 = {"DCDCLoThCh3", &RegisterDCDCLoTh1, 0, 10, 0x0};
    DCDCLoThCh4 = {"DCDCLoThCh4", &RegisterDCDCLoTh1, 10, 10, 0x0};
    DCDCLoThCh5 = {"DCDCLoThCh5", &RegisterDCDCLoTh1, 20, 10, 0x0};
    // 150 - DCDCLoTh2
    DCDCLoThCh6 = {"DCDCLoThCh6", &RegisterDCDCLoTh2, 0, 10, 0x0};
    DCDCLoThCh7 = {"DCDCLoThCh7", &RegisterDCDCLoTh2, 10, 10, 0x0};
    DCDCLoThCh8 = {"DCDCLoThCh8", &RegisterDCDCLoTh2, 20, 10, 0x0};
    // 151 - DCDCLoTh3
    DCDCLoThCh9 = {"DCDCLoThCh9", &RegisterDCDCLoTh3, 0, 10, 0x0};
    DCDCLoThCh10 = {"DCDCLoThCh10", &RegisterDCDCLoTh3, 10, 10, 0x0};
    DCDCLoThCh11 = {"DCDCLoThCh11", &RegisterDCDCLoTh3, 20, 10, 0x0};
    // 152 - DCDCLoTh4
    DCDCLoThCh12 = {"DCDCLoThCh12", &RegisterDCDCLoTh4, 0, 10, 0x0};
    DCDCLoThCh13 = {"DCDCLoThCh13", &RegisterDCDCLoTh4, 10, 10, 0x0};
    DCDCLoThCh14 = {"DCDCLoThCh14", &RegisterDCDCLoTh4, 20, 10, 0x0};
    // 153 - DCDCLoTh5
    DCDCLoThCh15 = {"DCDCLoThCh15", &RegisterDCDCLoTh5, 0, 10, 0x0};
    // 154 - DCDCHiTh0
    DCDCHiThCh0 = {"DCDCHiThCh0", &RegisterDCDCHiTh0, 0, 10, 0x3FF};
    DCDCHiThCh1 = {"DCDCHiThCh1", &RegisterDCDCHiTh0, 10, 10, 0x3FF};
    DCDCHiThCh2 = {"DCDCHiThCh2", &RegisterDCDCHiTh0, 20, 10, 0x3FF};
    // 155 - DCDCHiTh1
    DCDCHiThCh3 = {"DCDCHiThCh3", &RegisterDCDCHiTh1, 0, 10, 0x3FF};
    DCDCHiThCh4 = {"DCDCHiThCh4", &RegisterDCDCHiTh1, 10, 10, 0x3FF};
    DCDCHiThCh5 = {"DCDCHiThCh5", &RegisterDCDCHiTh1, 20, 10, 0x3FF};
    // 156 - DCDCHiTh2
    DCDCHiThCh6 = {"DCDCHiThCh6", &RegisterDCDCHiTh2, 0, 10, 0x3FF};
    DCDCHiThCh7 = {"DCDCHiThCh7", &RegisterDCDCHiTh2, 10, 10, 0x3FF};
    DCDCHiThCh8 = {"DCDCHiThCh8", &RegisterDCDCHiTh2, 20, 10, 0x3FF};
    // 157 - DCDCHiTh3
    DCDCHiThCh9 = {"DCDCHiThCh9", &RegisterDCDCHiTh3, 0, 10, 0x3FF};
    DCDCHiThCh10 = {"DCDCHiThCh10", &RegisterDCDCHiTh3, 10, 10, 0x3FF};
    DCDCHiThCh11 = {"DCDCHiThCh11", &RegisterDCDCHiTh3, 20, 10, 0x3FF};
    // 158 - DCDCHiTh4
    DCDCHiThCh12 = {"DCDCHiThCh12", &RegisterDCDCHiTh4, 0, 10, 0x3FF};
    DCDCHiThCh13 = {"DCDCHiThCh13", &RegisterDCDCHiTh4, 10, 10, 0x3FF};
    DCDCHiThCh14 = {"DCDCHiThCh14", &RegisterDCDCHiTh4, 20, 10, 0x3FF};
    // 159 - DCDCHiTh5
    DCDCHiThCh15 = {"DCDCHiThCh15", &RegisterDCDCHiTh5, 0, 10, 0x3FF};
    // 160 - WRNLoTh0
    WRNLoThCh0 = {"WRNLoThCh0", &RegisterWRNLoTh0, 0, 10, 0x0};
    WRNLoThCh1 = {"WRNLoThCh1", &RegisterWRNLoTh0, 10, 10, 0x0};
    WRNLoThCh2 = {"WRNLoThCh2", &RegisterWRNLoTh0, 20, 10, 0x0};
    // 161 - WRNLoTh1
    WRNLoThCh3 = {"WRNLoThCh3", &RegisterWRNLoTh1, 0, 10, 0x0};
    WRNLoThCh4 = {"WRNLoThCh4", &RegisterWRNLoTh1, 10, 10, 0x0};
    WRNLoThCh5 = {"WRNLoThCh5", &RegisterWRNLoTh1, 20, 10, 0x0};
    // 162 - WRNLoTh2
    WRNLoThCh6 = {"WRNLoThCh6", &RegisterWRNLoTh2, 0, 10, 0x0};
    WRNLoThCh7 = {"WRNLoThCh7", &RegisterWRNLoTh2, 10, 10, 0x0};
    WRNLoThCh8 = {"WRNLoThCh8", &RegisterWRNLoTh2, 20, 10, 0x0};
    // 163 - WRNLoTh3
    WRNLoThCh9 = {"WRNLoThCh9", &RegisterWRNLoTh3, 0, 10, 0x0};
    WRNLoThCh10 = {"WRNLoThCh10", &RegisterWRNLoTh3, 10, 10, 0x0};
    WRNLoThCh11 = {"WRNLoThCh11", &RegisterWRNLoTh3, 20, 10, 0x0};
    // 164 - WRNLoTh4
    WRNLoThCh12 = {"WRNLoThCh12", &RegisterWRNLoTh4, 0, 10, 0x0};
    WRNLoThCh13 = {"WRNLoThCh13", &RegisterWRNLoTh4, 10, 10, 0x0};
    WRNLoThCh14 = {"WRNLoThCh14", &RegisterWRNLoTh4, 20, 10, 0x0};
    // 165 - WRNLoTh5
    WRNLoThCh15 = {"WRNLoThCh15", &RegisterWRNLoTh5, 0, 10, 0x0};
    // 166 - WRNHiTh0
    WRNHiThCh0 = {"WRNHiThCh0", &RegisterWRNHiTh0, 0, 10, 0x3FF};
    WRNHiThCh1 = {"WRNHiThCh1", &RegisterWRNHiTh0, 10, 10, 0x3FF};
    WRNHiThCh2 = {"WRNHiThCh2", &RegisterWRNHiTh0, 20, 10, 0x3FF};
    // 167 - WRNHiTh1
    WRNHiThCh3 = {"WRNHiThCh3", &RegisterWRNHiTh1, 0, 10, 0x3FF};
    WRNHiThCh4 = {"WRNHiThCh4", &RegisterWRNHiTh1, 10, 10, 0x3FF};
    WRNHiThCh5 = {"WRNHiThCh5", &RegisterWRNHiTh1, 20, 10, 0x3FF};
    // 168 - WRNHiTh2
    WRNHiThCh6 = {"WRNHiThCh6", &RegisterWRNHiTh2, 0, 10, 0x3FF};
    WRNHiThCh7 = {"WRNHiThCh7", &RegisterWRNHiTh2, 10, 10, 0x3FF};
    WRNHiThCh8 = {"WRNHiThCh8", &RegisterWRNHiTh2, 20, 10, 0x3FF};
    // 169 - WRNHiTh3
    WRNHiThCh9 = {"WRNHiThCh9", &RegisterWRNHiTh3, 0, 10, 0x3FF};
    WRNHiThCh10 = {"WRNHiThCh10", &RegisterWRNHiTh3, 10, 10, 0x3FF};
    WRNHiThCh11 = {"WRNHiThCh11", &RegisterWRNHiTh3, 20, 10, 0x3FF};
    // 170 - WRNHiTh4
    WRNHiThCh12 = {"WRNHiThCh12", &RegisterWRNHiTh4, 0, 10, 0x3FF};
    WRNHiThCh13 = {"WRNHiThCh13", &RegisterWRNHiTh4, 10, 10, 0x3FF};
    WRNHiThCh14 = {"WRNHiThCh14", &RegisterWRNHiTh4, 20, 10, 0x3FF};
    // 171 - WRNHiTh5
    WRNHiThCh15 = {"WRNHiThCh15", &RegisterWRNHiTh5, 0, 10, 0x3FF};

    /** @} */
}
